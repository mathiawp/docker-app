// @flow
import express from 'express';
import codeService from './code-service';

/**
 * Express router containing task methods.
 */
const router: express$Router<> = express.Router();

router.post('/run', (request, response) => {
  const data = request.body;

  codeService
    .run(data.codeAsText)
    .then((output) => response.send(output))
    .catch((error: Error) => response.status(500).send(error));
});


export default router;
