// @flow
const util = require('util');
const exec = util.promisify(require('child_process').exec);

class CodeService {
  async run(code: string) {
    try {
      /**
       * Doing some finessing with the code that gets sent from the client:
       * 1. Add a \ before every "-sign so that users can declare variables with whatever apostrophe they want.
       * 2. Remove all line-breaks so that cmd understands wtf is going on.
       */
      const transformedDockerCode = `docker run --rm node-image node -e "${code.replace(/"/gm, '\\"')}"`.replace(/(\r\n|\n|\r)/gm, "");
      const { stderr, stdout } = await exec(transformedDockerCode);
      return {
        output: stdout,
        error: stderr
      }
    } catch (error) {
      return {
        error: error.stderr
      }
    }
  }
}

const codeService = new CodeService();
export default codeService;
