// @flow

import express from 'express';
import codeRouter from './code-router';

/**
 * Express application.
 */
const app: express$Application<> = express();

app.use(express.json());

app.use('/api/v1', codeRouter);

export default app;
