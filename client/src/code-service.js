// @flow
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v1';


class CodeService {

    run(code: string) {
        return axios.post<{}, { codeAsText: string }>('/run', { codeAsText: code }).then((response) => response);
    }
}

const codeService = new CodeService();
export default codeService;
