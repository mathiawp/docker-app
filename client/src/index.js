// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { Card, Row, Column, Form, Button, Output } from './widgets';
import codeService from './code-service';

class CodeInput extends Component {
  code = "";
  loading = false;
  codeReturn = {
    output: "",
    error: "",
  }

  runCode(code) {
    this.loading = true;
    this.codeReturn.output = "";
    this.codeReturn.error = "";
    codeService.run(this.code).then(response => {
      this.codeReturn = response.data
    }).finally(() => this.loading = false)
  }

  render() {
    return (
      <>
        <Card title="app.js" >
          <div style={{ display: "flex", flexDirection: "column" }}>
            <textarea rows={6} cols={40} onChange={(event) => this.code = event.currentTarget.value}></textarea>
            <Button.Success
              onClick={this.runCode}
            >
              {this.loading ? "Running code..." : "Run code"}
            </Button.Success>
          </div>
        </Card>
        {
          this.loading ?
            <div style={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
              <h2>Just give me a couple of seconds...</h2>
              <img src="https://i.pinimg.com/originals/45/a3/31/45a33135fff0f5920a643b7e61bd731a.gif" />
            </div>
            : null
        }
        <Output.Standard output={this.codeReturn.output} />
        <Output.Error output={this.codeReturn.error} />
        <Card title={`Exit status: ${this.codeReturn.error ? 1 : 0}`} />
      </>
    );
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <>
      <CodeInput />
    </>,
    root
  );
